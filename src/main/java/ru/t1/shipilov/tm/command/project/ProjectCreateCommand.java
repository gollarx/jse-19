package ru.t1.shipilov.tm.command.project;

import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private final String NAME = "project-create";

    private final String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
