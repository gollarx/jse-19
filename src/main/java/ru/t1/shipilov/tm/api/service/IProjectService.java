package ru.t1.shipilov.tm.api.service;

import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name, String description);

    Project create(String name);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
