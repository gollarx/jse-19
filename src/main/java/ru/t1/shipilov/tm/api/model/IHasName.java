package ru.t1.shipilov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
